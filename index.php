<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>
		<p><?php whileLoop()?></p>

		<h2>Do While Loop</h2>
		<p><?php doWhileLoop()?></p>

		<h2>For Loop</h2>
		<p><?php forLoop()?></p>

		<h2>Continue and Break Statement</h2>
		<p><?php modifiedForLoop()?></p>


		<h1>Array Manipulation</h1>

		<h2>Types of Arrays</h2>

		<h3>Simple Arrays</h3>
		<!-- 
			foreach
				- This loop only works on arrays
				- Loops through each key/value pair in an array.

			Syntax (Simple array):
			foreach($array as $element){
				//code block to be executed
			}
		 -->
		<ul>
			<!-- php codes/statements can be broken down using the php tags if it is incorporated with html tags. -->
			<?php foreach($computerBrands as $brand){?>
				<!-- PHP includes a short hand method for "php echo" tag -->
				<li><?=$brand?></li>
			<?php } ?>
		</ul>

		<h3>Associative Arrays</h3>
		<!-- 
			foreach Syntax(associative array):

			foreach($array as $key => $value){
				// code to be executed
			};
		 -->

		 <ul>
		 	<?php foreach($gradesPeriod as $period => $grade) {?>
		 		<li>
		 			Grade in <?=$period?> is <?=$grade?>
		 		</li>
		 	<?php }?>
		 </ul>

		 <h3>Multidimensional Arrays</h3>
		 <ul>
		 	<?php 
		 	// Each heroes will be represented by a $team(outer array)
		 		foreach($heroes as $team){
		 			// Each team will be represented as $member(inner array)
		 			foreach($team as $member){
		 	?>
		 	<li><?= $member?></li>
		 	<?php 
		 			}
		 		}
		 	?>
		 </ul>

		 <h3>Multidimensional Associative Arrays</h3>
		 <ul>
		 	<?php 
		 		foreach($ironManPowers as $label => $powers){
		 			foreach($powers as $power){
		 	?>
		 	<li><?= "$label: $power"?></li>
		 	<?php 
		 			}
		 		}
		 	?>
		 </ul>


		 <h2>Array Functions</h2>

		 <h3>Original Array</h3>
		 <pre><?php print_r($computerBrands)?></pre>

		 <h3>Sorted Array</h3>
		 <h4>Sorting in Ascending Order</h4>
		 <pre><?php print_r($sortedBrands)?></pre>

		 <h4>Sorting in Reversed Order</h4>
		 <pre><?php print_r($reversedSortedBrands)?></pre>

		 <h3>Append</h3>

		 <!-- array_push -->
		 <h4>Add one or more element on the end of an array.</h4>
		 <?php array_push($computerBrands, "Apple");?>
		 <pre><?php print_r($computerBrands)?></pre>

		 <!-- array_unshift -->
		 <h4>Add one or more element at the start of an array.</h4>
		 <?php array_unshift($computerBrands, "Dell");?>
		 <pre><?php print_r($computerBrands)?></pre>


		 <h3>Remove</h3>

		 <!-- array_pop -->
		 <h4>Remove the element off in the end of an array</h4>
		 <?php array_pop($computerBrands);?>
		 <pre><?php print_r($computerBrands)?></pre>

		 <!-- array_shift -->
		 <h4>Remove the element off at the start of an array</h4>
		 <?php array_shift($computerBrands);?>
		 <pre><?php print_r($computerBrands)?></pre>


		 <h3>Others</h3>

		 <!-- count() -->
		 <h4>Counts the number of elements</h4>
		 <pre><?php echo count($computerBrands)?></pre>

		 <!-- in_array -->
		 <h4>in_array: is used to check if the specific element exists in the array</h4>
		 <p><?php echo searchBrand($computerBrands, "HP")?></p>
		 <p><?php echo searchBrand($computerBrands, "Asus")?></p>

		 <!-- array_reverse() -->
		 <h4>array_reverse: returns the array in the reversed order</h4>
		 <pre><?php print_r($gradesPeriod)?></pre>
		 <pre><?php print_r($reversedGradePeriods)?></pre>
	</body>
</html>